from flask import Flask, render_template
from flask import jsonify
import copy

app = Flask(__name__)

@app.route('/dashboard')
@app.route('/')
def dashboard():
    return render_template('dashboard.html')

@app.route('/graph')
def graph():
    return render_template('graph.html')


@app.route('/table')
def table():
    return render_template('table.html')


@app.route('/data')
def data():
    s = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 10]
    t = {}
    t['x'] = copy.deepcopy(s)
    t['y'] = copy.deepcopy(s)
    t['y'][5]=20
    return jsonify(t)


@app.route('/table_data')
def table_data():
    data = [["1234567","ctyjhgj,k","tjrchgkvj","trdjcykvubli","bgriukberigb"],
            ["2345678","kuk,yty","ewgfah","ahrae","bgriukbe rigb"],
            ["3456789","wefgwge,k","tjrchgkvj","trdjcykvubli","bgriukberigb"]]
    return jsonify(data)


app.run(debug=True)
